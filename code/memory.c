//
// Created by Lenovo on 21.10.2023.
//

#include "memory.h"


size_t memorySize = 0;


void* allocMemory(size_t numOfElement, size_t sizeOfElement, myError* err){
    void* mem = calloc(numOfElement, sizeOfElement);
    if(mem == NULL){
        *err = ERROR_MEMORY;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    memorySize += numOfElement * sizeOfElement;
    return mem;
}
