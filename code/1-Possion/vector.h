//
// Created by Lenovo on 21.10.2023.
//

#ifndef PARALAB_VECTOR_H
#define PARALAB_VECTOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "../error.h"
#include "../memory.h"


typedef struct vector {
    size_t n;
    double* data;
} vector;


vector* creatVector(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void copyData(vector* from, vector* to, myError* err);


double dotProduct_series(vector* v1, vector* v2, myError* err);


double dotProduct_para(vector* v1, vector* v2, int rank, int size, myError* err);

// z = ax + y
vector*
saxpy_series(double a, vector* v1, vector* v2, void* const (* allocator)(size_t, size_t, myError*), myError* err);

// z = ax + y
vector*
saxpy_para(double a, vector* v1, vector* v2, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
           myError* err);


void deleteVector(vector* vec, myError* err);


void paintVector(vector* vec, myError* err);


#endif //PARALAB_VECTOR_H
