//
// Created by Lenovo on 25.10.2023.
//

#include "myMath.h"

vector*
linspace(double start, double end, size_t num, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    double h = (end - start) / (double) (num - 1);
    vector* vec = creatVector(num, allocator, err);
    if (vec == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    for (size_t i = 0; i < num; ++i) {
        vec->data[i] = start + (double) i * h;
    }
    return vec;
}


double normInf(vector* vec, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return -1; // ERROR ?
    }
    double val = fabs(vec->data[0]);
    double temp;
    for (size_t i = 1; i < vec->n; ++i) {
        temp = fabs(vec->data[i]);
        if (temp > val)
            val = temp;
    }
    return val;
}


double norm2(vector* vec, int rank, int size, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return -1; // ERROR ?
    }
    return sqrt(dotProduct_para(vec, vec, rank, size, err));
}
