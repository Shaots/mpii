//
// Created by Lenovo on 21.10.2023.
//

#include "elliptic.h"


double u(double x, double y) {
    return cos(PI * x) * sin(PI * y);
    // return x * x * y * y + 1;
}


double f(double x, double y) {
    return 2 * PI * PI * cos(PI * x) * sin(PI * y);
    // return -1 * (x * x + y * y);
}


ellipticProb*
creatEllipticProb(square sq, size_t n, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
                  myError* err) {
    size_t dim = n - 2;
    ellipticProb* ell;
    if (allocator == NULL || n < 5) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    ell = allocator(1, sizeof(ellipticProb), err);
    if (ell == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t numNodeAtBoundary = 4 * (dim - 2);
    ell->indexLeft = (size_t*) allocator(numNodeAtBoundary, sizeof(size_t), err);
    if (ell->indexLeft == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        free(ell);
        return NULL;
    }

    ell->indexDown = ell->indexLeft + (dim - 2);
    ell->indexUp = ell->indexDown + (dim - 2);
    ell->indexRight = ell->indexUp + (dim - 2);
    ell->iA.leftDown = 0;
    ell->iA.leftUp = dim - 1;
    ell->iA.rightDown = dim * (dim - 1);
    ell->iA.rightUp = dim * dim - 1;
    ell->sq = sq;
    ell->n = n;
    ell->h = ell->sq.sideLength / (double) (ell->n - 1);


    size_t edge1 = dim - 2;
    size_t edge2 = 2 * edge1;
    size_t edge3 = 3 * edge1;
    size_t k = 4 * (dim - 2) / size;    // Каждый процесс берет ответственность на k чисел.
    size_t r = 4 * (dim - 2) % size;    // Последний процесс берет ответственность на (k + r) чисел
    size_t num = k + r * (rank == size - 1);
    size_t* index = (size_t*) allocator(num, sizeof(size_t), err);
    if (index == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        if (rank == MPI_ROOT) {
            free(ell->indexLeft);
            free(ell);
        }
        return NULL;
    }
    for (size_t i = 0; i < num; ++i) {
        size_t currentPosition = rank * k + i;
        if (0 <= currentPosition && currentPosition < edge1) {
            index[i] = currentPosition + 1;
        }
        if (edge1 <= currentPosition && currentPosition < edge2) {
            index[i] = (currentPosition - edge1) * dim + dim;
        }
        if (edge2 <= currentPosition && currentPosition < edge3) {
            index[i] = (currentPosition - edge2) * dim + 2 * dim - 1;
        }
        if (edge3 <= currentPosition) {
            index[i] = (currentPosition - edge3) + dim * (dim - 1) + 1;
        }
    }


    int* displacement = (int*) allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        free(index);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int* receiveCounts = (int*) allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        free(index);
        free(displacement);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int duplicate = (int) num;
    MPI_Allgather(&duplicate, 1, MPI_INT,
                  receiveCounts, 1, MPI_INT, MPI_COMM_WORLD);
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    MPI_Allgatherv(index, (int) num, MY_MPI_SIZE_T,
                   ell->indexLeft, receiveCounts, displacement, MY_MPI_SIZE_T, MPI_COMM_WORLD);
    free(index);
    free(receiveCounts);
    free(displacement);
    return ell;
}


sparseMatrix*
calStiffness(ellipticProb* ell, int rank, int size, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (ell == NULL || ell->indexLeft == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = ell->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    sparseMatrix* sp = createSparseMatrix(numNoZero, allocator, err);
    if (sp == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) столбцов
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k столбцов.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);

    size_t counter = rank * k * dim;
    size_t counterLeft = 0;
    size_t counterDown;
    size_t counterUp;
    size_t counterRight = 0;
    double diameter = ell->h;


    double* dataSp;
    size_t* colSp;
    size_t* rowSp;
    size_t num;

    if (rank == 0) {
        if (size == 1) {
            num = numNoZero;
        } else {
            // 2 угла + (dim - 2) граничных узлов + 2 * (k - 1) граничных узлов + (k - 1) столбцов * (dim - 2) внутренних узлов
            num = 2 * 3 + (dim - 2) * 4 + 2 * (k - 1) * 4 + (k - 1) * (dim - 2) * 5;
        }
        dataSp = (double*) allocator(num, sizeof(double), err);
        if (dataSp == NULL) {
            free(sp);
            reportError(*err, __FILE__, __FUNCTION__, __LINE__);
            return NULL;
        }
        colSp = (size_t*) allocator(num, sizeof(size_t), err);
        if (colSp == NULL) {
            free(sp);
            free(dataSp);
            reportError(*err, __FILE__, __FUNCTION__, __LINE__);
            return NULL;
        }
        rowSp = (size_t*) allocator(num, sizeof(size_t), err);
        if (rowSp == NULL) {
            free(sp);
            free(dataSp);
            free(colSp);
            reportError(*err, __FILE__, __FUNCTION__, __LINE__);
            return NULL;
        }
        counterDown = 0;
        counterUp = 0;
    } else {
        if (rank == size - 1) {
            // 2 * (k + r - 1) граничных узлов + (k + r - 1) столбцов * (dim - 2) внутренних узлов + 2 угла + (dim - 2) граничных узлов
            num = 2 * (k + r - 1) * 4 + (k + r - 1) * (dim - 2) * 5 + 2 * 3 + (dim - 2) * 4;

            dataSp = (double*) allocator(num, sizeof(double), err);
            if (dataSp == NULL) {
                free(sp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
            colSp = (size_t*) allocator(num, sizeof(size_t), err);
            if (colSp == NULL) {
                free(sp);
                free(dataSp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
            rowSp = (size_t*) allocator(num, sizeof(size_t), err);
            if (rowSp == NULL) {
                free(sp);
                free(dataSp);
                free(colSp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
        } else {
            //  2 * k граничных узлов + k столбцов * (dim - 2) внутренних узлов
            num = 2 * k * 4 + k * (dim - 2) * 5;

            dataSp = (double*) allocator(num, sizeof(double), err);
            if (dataSp == NULL) {
                free(sp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
            colSp = (size_t*) allocator(num, sizeof(size_t), err);
            if (colSp == NULL) {
                free(sp);
                free(dataSp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
            rowSp = (size_t*) allocator(num, sizeof(size_t), err);
            if (rowSp == NULL) {
                free(sp);
                free(dataSp);
                free(colSp);
                reportError(*err, __FILE__, __FUNCTION__, __LINE__);
                return NULL;
            }
        }
        size_t numFirstProcDown = k - 1;
        size_t numInnerProcDown = (rank - 1) * k;
        counterDown = numFirstProcDown + numInnerProcDown;

        size_t numFirstProcUp = k - 1;
        size_t numInnerProcUp = (rank - 1) * k;
        counterUp = numFirstProcUp + numInnerProcUp;
    }


    int* displacement = allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        free(sp);
        free(dataSp);
        free(colSp);
        free(rowSp);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int* receiveCounts = allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        free(sp);
        free(dataSp);
        free(colSp);
        free(rowSp);
        free(displacement);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int duplicate = (int) num;
    MPI_Allgather(&duplicate, 1, MPI_INT,
                  receiveCounts, 1, MPI_INT, MPI_COMM_WORLD);
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    size_t myCounter = 0;
    for (size_t col = start; col < end; ++col) {
        for (size_t row = 0; row < dim; ++row) {
            if (counter == ell->iA.leftDown) {
                // Left down angle

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;

                counter++;
                continue;
            }
            if (counter == ell->indexLeft[counterLeft]) {
                // Left
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;

                counter++;
                counterLeft++;
                if (counterLeft == dim - 2) {
                    counterLeft = 0;
                }
                continue;
            }
            if (counter == ell->iA.leftUp) {
                // Left up angle
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;

                counter++;
                continue;
            }
            if (counter == ell->indexDown[counterDown]) {
                // Down
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;

                counter++;
                counterDown++;
                if (counterDown == dim - 2) {
                    counterDown = 0;
                }
                continue;
            }
            if (counter == ell->indexUp[counterUp]) {
                // Up
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;

                counter++;
                counterUp++;
                if (counterUp == dim - 2) {
                    counterUp = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightDown) {
                // Right down angle
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                counter++;
                continue;
            }
            if (counter == ell->indexRight[counterRight]) {
                // Right
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                counter++;
                counterRight++;
                if (counterRight == dim - 2) {
                    counterRight = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightUp) {
                // Right up angle
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                counter++;
                continue;
            } else {
                // inner
                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - dim;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter - 1;
                myCounter++;

                dataSp[myCounter] = 4 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + 1;
                myCounter++;

                dataSp[myCounter] = -1 / (diameter * diameter);
                rowSp[myCounter] = counter;
                colSp[myCounter] = counter + dim;
                myCounter++;
                counter++;
            }
        }
    }

    MPI_Allgatherv(dataSp, (int) num, MPI_DOUBLE,
                   sp->vec->data, receiveCounts, displacement, MPI_DOUBLE, MPI_COMM_WORLD);
    MPI_Allgatherv(colSp, (int) num, MY_MPI_SIZE_T,
                   sp->colIndex, receiveCounts, displacement, MY_MPI_SIZE_T, MPI_COMM_WORLD);
    MPI_Allgatherv(rowSp, (int) num, MY_MPI_SIZE_T,
                   sp->rowIndex, receiveCounts, displacement, MY_MPI_SIZE_T, MPI_COMM_WORLD);

    free(dataSp);
    free(colSp);
    free(rowSp);
    free(receiveCounts);
    free(displacement);
    return sp;
}


vector*
calForce(ellipticProb* ell, int rank, int size, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (ell == NULL || ell->indexLeft == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = ell->n - 2;

    double startX = ell->sq.leftBottom.x;
    double startY = ell->sq.leftBottom.y;
    double sizeLen = ell->sq.sideLength;
    if (startX == NAN || startY == NAN || sizeLen == NAN) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* X = linspace(startX, startX + sizeLen, ell->n, allocator, err);
    if (X == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* Y = linspace(startY, startY + sizeLen, ell->n, allocator, err);
    if (Y == NULL) {
        deleteVector(X, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* force = creatVector(dim * dim, allocator, err);
    if (force == NULL) {
        deleteVector(X, err);
        deleteVector(Y, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }


    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) столбцов
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k столбцов.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);

    size_t counter = rank * k * dim;
    size_t counterLeft = 0;
    size_t counterDown;
    size_t counterUp;
    size_t counterRight = 0;
    double diameter = ell->h;

    size_t num = (end - start) * dim;
    double* localForce = (double*) allocator(num, sizeof(double), err);

    if (rank == 0) {
        counterDown = 0;
        counterUp = 0;
    } else {
        size_t numFirstProcDown = k - 1;
        size_t numInnerProcDown = (rank - 1) * k;
        counterDown = numFirstProcDown + numInnerProcDown;

        size_t numFirstProcUp = k - 1;
        size_t numInnerProcUp = (rank - 1) * k;
        counterUp = numFirstProcUp + numInnerProcUp;
    }

    int* displacement = allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        deleteVector(X, err);
        deleteVector(Y, err);
        deleteVector(force, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int* receiveCounts = allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        deleteVector(X, err);
        deleteVector(Y, err);
        deleteVector(force, err);
        free(receiveCounts);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int duplicate = (int) num;
    MPI_Allgather(&duplicate, 1, MPI_INT,
                  receiveCounts, 1, MPI_INT, MPI_COMM_WORLD);
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    size_t myCounter = 0;
    for (size_t col = start; col < end; ++col) {
        for (size_t row = 0; row < dim; ++row) {
            double x = X->data[col + 1];
            double y = Y->data[row + 1];
            if (counter == ell->iA.leftDown) {
                // Left down angle
                double xLeft = X->data[col];
                double yDown = Y->data[row];
                localForce[myCounter++] =
                        f(x, y) + 1 / (diameter * diameter) * u(xLeft, y) + 1 / (diameter * diameter) * u(x, yDown);
                counter++;
                continue;
            }
            if (counter == ell->indexLeft[counterLeft]) {
                // Left
                double xLeft = X->data[col];
                localForce[myCounter++] = f(x, y) + 1 / (diameter * diameter) * u(xLeft, y);
                counter++;
                counterLeft++;
                if (counterLeft == dim - 2) {
                    counterLeft = 0;
                }
                continue;
            }
            if (counter == ell->iA.leftUp) {
                // Left up angle
                double xLeft = X->data[col];
                double yUp = Y->data[row + 2];
                localForce[myCounter++] =
                        f(x, y) + 1 / (diameter * diameter) * u(xLeft, y) + 1 / (diameter * diameter) * u(x, yUp);
                counter++;
                continue;
            }
            if (counter == ell->indexDown[counterDown]) {
                // Down
                double yDown = Y->data[row];
                localForce[myCounter++] = f(x, y) + 1 / (diameter * diameter) * u(x, yDown);
                counter++;
                counterDown++;
                if (counterDown == dim - 2) {
                    counterDown = 0;
                }
                continue;
            }
            if (counter == ell->indexUp[counterUp]) {
                // Up
                double yUp = Y->data[row + 2];
                localForce[myCounter++] = f(x, y) + 1 / (diameter * diameter) * u(x, yUp);
                counter++;
                counterUp++;
                if (counterUp == dim - 2) {
                    counterUp = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightDown) {
                // Right down angle
                double xRight = X->data[col + 2];
                double yDown = Y->data[row];
                localForce[myCounter++]
                        = f(x, y) + 1 / (diameter * diameter) * u(x, yDown) + 1 / (diameter * diameter) * u(xRight, y);
                counter++;
                continue;
            }
            if (counter == ell->indexRight[counterRight]) {
                // Right
                double xRight = X->data[col + 2];
                localForce[myCounter++] = f(x, y) + 1 / (diameter * diameter) * u(xRight, y);
                counter++;
                counterRight++;
                if (counterRight == dim - 2) {
                    counterRight = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightUp) {
                // Right up angle
                double xRight = X->data[col + 2];
                double yUp = Y->data[row + 2];
                localForce[myCounter++] =
                        f(x, y) + 1 / (diameter * diameter) * u(x, yUp) + 1 / (diameter * diameter) * u(xRight, y);
                counter++;
                continue;
            } else {
                // inner
                localForce[myCounter++] = f(x, y);
                counter++;
            }
        }
    }

    MPI_Allgatherv(localForce, (int) num, MPI_DOUBLE,
                   force->data, receiveCounts, displacement, MPI_DOUBLE, MPI_COMM_WORLD);

    deleteVector(X, err);
    deleteVector(Y, err);
    free(receiveCounts);
    free(displacement);
    free(localForce);
    return force;
}

// A * x
vector* fiveDiagMatrixMultiplyVec(ellipticProb* ell, sparseMatrix* A, vector* x, int rank, int size,
                                  void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (ell == NULL || ell->indexLeft == NULL || A == NULL || x == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = ell->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    if (A->vec->n != numNoZero || x->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* vecGlobal = creatVector(dim * dim, allocator, err);
    if (vecGlobal == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) столбцов
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k столбцов
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);
    size_t num = (end - start) * dim;
    vector* vecLocal = creatVector(num, allocator, err);
    if (vecLocal == NULL) {
        deleteVector(vecGlobal, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int* displacement = allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        deleteVector(vecGlobal, err);
        deleteVector(vecLocal, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int* receiveCounts = allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        deleteVector(vecGlobal, err);
        deleteVector(vecLocal, err);
        free(displacement);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int duplicate = (int) num;
    MPI_Allgather(&duplicate, 1, MPI_INT,
                  receiveCounts, 1, MPI_INT, MPI_COMM_WORLD);
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    size_t counter = rank * k * dim;
    size_t counterSp;
    size_t counterLeft = 0;
    size_t counterDown;
    size_t counterUp;
    size_t counterRight = 0;

    if (rank == 0) {
        counterDown = 0;
        counterUp = 0;
        counterSp = 0;
    } else {
        size_t numFirstProcDown = k - 1;
        size_t numInnerProcDown = (rank - 1) * k;
        counterDown = numFirstProcDown + numInnerProcDown;

        size_t numFirstProcUp = k - 1;
        size_t numInnerProcUp = (rank - 1) * k;
        counterUp = numFirstProcUp + numInnerProcUp;

        size_t lenFirstProcess = 2 * 3 + (dim - 2) * 4 + 2 * (k - 1) * 4 + (k - 1) * (dim - 2) * 5;
        size_t lenInnerProcess = 2 * k * 4 + k * (dim - 2) * 5;
        counterSp = lenFirstProcess + (rank - 1) * lenInnerProcess;
    }

    size_t myCounter = 0;
    for (size_t col = start; col < end; ++col) {
        for (size_t row = 0; row < dim; ++row) {
            if (counter == ell->iA.leftDown) {
                // Left down angle
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter] +
                                            A->vec->data[counterSp + 1] * x->data[counter + 1] +
                                            A->vec->data[counterSp + 2] * x->data[counter + dim];
                counterSp += 3;
                counter++;
                myCounter++;
                continue;
            }
            if (counter == ell->indexLeft[counterLeft]) {
                // Left
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 1] * x->data[counter] +
                                            A->vec->data[counterSp + 2] * x->data[counter + 1] +
                                            A->vec->data[counterSp + 3] * x->data[counter + dim];
                counter++;
                counterSp += 4;
                myCounter++;
                counterLeft++;
                if (counterLeft == dim - 2) {
                    counterLeft = 0;
                }
                continue;
            }
            if (counter == ell->iA.leftUp) {
                // Left up angle
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 1] * x->data[counter] +
                                            A->vec->data[counterSp + 2] * x->data[counter + dim];
                counterSp += 3;
                counter++;
                myCounter++;
                continue;
            }
            if (counter == ell->indexDown[counterDown]) {
                // Down
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter] +
                                            A->vec->data[counterSp + 2] * x->data[counter + 1] +
                                            A->vec->data[counterSp + 3] * x->data[counter + dim];
                counterSp += 4;
                counter++;
                myCounter++;
                counterDown++;
                if (counterDown == dim - 2) {
                    counterDown = 0;
                }
                continue;
            }
            if (counter == ell->indexUp[counterUp]) {
                // Up
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 2] * x->data[counter] +
                                            A->vec->data[counterSp + 3] * x->data[counter + dim];
                counterSp += 4;
                counter++;
                myCounter++;
                counterUp++;
                if (counterUp == dim - 2) {
                    counterUp = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightDown) {
                // Right down angle
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter] +
                                            A->vec->data[counterSp + 2] * x->data[counter + 1];
                counterSp += 3;
                counter++;
                myCounter++;
                continue;
            }
            if (counter == ell->indexRight[counterRight]) {
                // Right
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 2] * x->data[counter] +
                                            A->vec->data[counterSp + 3] * x->data[counter + 1];
                counterSp += 4;
                counter++;
                myCounter++;
                counterRight++;
                if (counterRight == dim - 2) {
                    counterRight = 0;
                }
                continue;
            }
            if (counter == ell->iA.rightUp) {
                // Right up angle
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 2] * x->data[counter];
                counterSp += 3;
                counter++;
                myCounter++;
                continue;
            } else {
                // inner
                vecLocal->data[myCounter] = A->vec->data[counterSp] * x->data[counter - dim] +
                                            A->vec->data[counterSp + 1] * x->data[counter - 1] +
                                            A->vec->data[counterSp + 2] * x->data[counter] +
                                            A->vec->data[counterSp + 3] * x->data[counter + 1] +
                                            A->vec->data[counterSp + 4] * x->data[counter + dim];
                counterSp += 5;
                counter++;
                myCounter++;
            }
        }
    }

    MPI_Allgatherv(vecLocal->data, (int) num, MPI_DOUBLE,
                   vecGlobal->data, receiveCounts, displacement, MPI_DOUBLE, MPI_COMM_WORLD);

    deleteVector(vecLocal, err);
    free(displacement);
    free(receiveCounts);
    return vecGlobal;
}


vector* jacobi(ellipticProb* ell, sparseMatrix* A, vector* b, int rank, int size,
               void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (ell == NULL || ell->indexLeft == NULL || A == NULL || b == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = ell->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    if (A->vec->n != numNoZero || b->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* x1 = creatVector(dim * dim, allocator, err);
    if (x1 == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) строк
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k строк.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);
    size_t num = (end - start) * dim;

    vector* diff = creatVector(num, allocator, err);
    if (diff == NULL) {
        deleteVector(x1, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* localX2 = creatVector(num, allocator, err);
    if (localX2 == NULL) {
        deleteVector(x1, err);
        deleteVector(diff, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int* displacement = allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        deleteVector(x1, err);
        deleteVector(diff, err);
        deleteVector(localX2, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    int* receiveCounts = allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        deleteVector(x1, err);
        deleteVector(diff, err);
        deleteVector(localX2, err);
        free(displacement);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int duplicate = (int) num;
    MPI_Allgather(&duplicate, 1, MPI_INT,
                  receiveCounts, 1, MPI_INT, MPI_COMM_WORLD);
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    double localDiff = 0;
    double globalDiff = 0;
    double tol = pow(10, -12);
    int flag = SHAOTRUE;
    while (flag) {
        size_t counter = rank * k * dim;
        size_t counterLeft = 0;
        size_t counterDown;
        size_t counterUp;
        size_t counterRight = 0;
        size_t counterSp;
        if (rank == 0) {
            counterDown = 0;
            counterUp = 0;
            counterSp = 0;
        } else {
            size_t numFirstProcDown = k - 1;
            size_t numInnerProcDown = (rank - 1) * k;
            counterDown = numFirstProcDown + numInnerProcDown;

            size_t numFirstProcUp = k - 1;
            size_t numInnerProcUp = (rank - 1) * k;
            counterUp = numFirstProcUp + numInnerProcUp;

            size_t lenFirstProcess = 2 * 3 + (dim - 2) * 4 + 2 * (k - 1) * 4 + (k - 1) * (dim - 2) * 5;
            size_t lenInnerProcess = 2 * k * 4 + k * (dim - 2) * 5;
            counterSp = lenFirstProcess + (rank - 1) * lenInnerProcess;
        }

        // Основной цикл
        size_t myCounter = 0;
        for (size_t col = start; col < end; ++col) {
            for (size_t row = 0; row < dim; ++row) {
                if (counter == ell->iA.leftDown) {
                    // Left down angle
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp + 1] * x1->data[counter + 1] -
                                                A->vec->data[counterSp + 2] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 3;
                    counter++;
                    myCounter++;
                    continue;
                }
                if (counter == ell->indexLeft[counterLeft]) {
                    // Left
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - 1] -
                                                A->vec->data[counterSp + 2] * x1->data[counter + 1] -
                                                A->vec->data[counterSp + 3] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp + 1];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counter++;
                    counterSp += 4;
                    myCounter++;
                    counterLeft++;
                    if (counterLeft == dim - 2) {
                        counterLeft = 0;
                    }
                    continue;
                }
                if (counter == ell->iA.leftUp) {
                    // Left up angle
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - 1] -
                                                A->vec->data[counterSp + 2] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp + 1];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 3;
                    counter++;
                    myCounter++;
                    continue;
                }
                if (counter == ell->indexDown[counterDown]) {
                    // Down
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 2] * x1->data[counter + 1] -
                                                A->vec->data[counterSp + 3] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp + 1];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 4;
                    counter++;
                    myCounter++;
                    counterDown++;
                    if (counterDown == dim - 2) {
                        counterDown = 0;
                    }
                    continue;
                }
                if (counter == ell->indexUp[counterUp]) {
                    // Up
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 1] * x1->data[counter - 1] -
                                                A->vec->data[counterSp + 3] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp + 2];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 4;
                    counter++;
                    myCounter++;
                    counterUp++;
                    if (counterUp == dim - 2) {
                        counterUp = 0;
                    }
                    continue;
                }
                if (counter == ell->iA.rightDown) {
                    // Right down angle
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 2] * x1->data[counter + 1]) /
                                               A->vec->data[counterSp + 1];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 3;
                    counter++;
                    myCounter++;
                    continue;
                }
                if (counter == ell->indexRight[counterRight]) {
                    // Right
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 1] * x1->data[counter - 1] -
                                                A->vec->data[counterSp + 3] * x1->data[counter + 1]) /
                                               A->vec->data[counterSp + 2];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 4;
                    counter++;
                    myCounter++;
                    counterRight++;
                    if (counterRight == dim - 2) {
                        counterRight = 0;
                    }
                    continue;
                }
                if (counter == ell->iA.rightUp) {
                    // Right up angle
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 1] * x1->data[counter - 1]) /
                                               A->vec->data[counterSp + 2];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 3;
                    counter++;
                    myCounter++;
                    continue;
                } else {
                    // inner
                    localX2->data[myCounter] = (b->data[counter] - A->vec->data[counterSp] * x1->data[counter - dim] -
                                                A->vec->data[counterSp + 1] * x1->data[counter - 1] -
                                                A->vec->data[counterSp + 3] * x1->data[counter + 1] -
                                                A->vec->data[counterSp + 4] * x1->data[counter + dim]) /
                                               A->vec->data[counterSp + 2];
                    diff->data[myCounter] = localX2->data[myCounter] - x1->data[counter];
                    counterSp += 5;
                    counter++;
                    myCounter++;
                }
            }
        }
        localDiff = normInf(diff, err);
        MPI_Allreduce(&localDiff, &globalDiff, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        flag = globalDiff > tol;
        MPI_Allgatherv(localX2->data, (int) num, MPI_DOUBLE,
                       x1->data, receiveCounts, displacement, MPI_DOUBLE, MPI_COMM_WORLD);
    }
    deleteVector(localX2, err);
    deleteVector(diff, err);
    free(receiveCounts);
    free(displacement);
    return x1;
}


vector* conjGrad(ellipticProb* ell, sparseMatrix* A, vector* b, int rank, int size,
                 void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (ell == NULL || ell->indexLeft == NULL || A == NULL || b == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = ell->n - 2;
    size_t numNodeAtAngle = 4;
    size_t numNodeAtSide = 4 * (dim - 2);
    size_t numNodeAtInternal = (dim - 2) * (dim - 2);
    size_t numNoZero = 5 * numNodeAtInternal + 4 * numNodeAtSide + 3 * numNodeAtAngle;
    if (A->vec->n != numNoZero || b->n != dim * dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* x = creatVector(dim * dim, allocator, err);
    if (x == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    vector* r1 = creatVector(dim * dim, allocator, err);
    if (r1 == NULL) {
        deleteVector(x, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    copyData(b, r1, err);

    vector* z = creatVector(dim * dim, allocator, err);
    if (z == NULL) {
        deleteVector(x, err);
        deleteVector(r1, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    copyData(r1, z, err);

    int flag = SHAOTRUE;
    vector* temp1;
    vector* r2;
    double tol = pow(10, -12);
    double alpha = 0;
    double beta = 0;
    while (flag) {

        temp1 = fiveDiagMatrixMultiplyVec(ell, A, z, rank, size, allocator, err);
        alpha = dotProduct_para(r1, r1, rank, size, err) / dotProduct_para(temp1, z, rank, size, err);
        deleteVector(temp1, err);

        // x = alpha * z + x;
        temp1 = saxpy_para(alpha, z, x, rank, size, allocator, err);
        copyData(temp1, x, err);
        deleteVector(temp1, err);

        // r2 = -alpha * A * z + r1;
        temp1 = fiveDiagMatrixMultiplyVec(ell, A, z, rank, size, allocator, err);
        r2 = saxpy_para(-alpha, temp1, r1, rank, size, allocator, err);
        deleteVector(temp1, err);

        beta = dotProduct_para(r2, r2, rank, size, err) / dotProduct_para(r1, r1, rank, size, err);

        // z = beta * z + r2;
        temp1 = z;
        z = saxpy_para(beta, z, r2, rank, size, allocator, err);
        deleteVector(temp1, err);

        flag = (norm2(r2, rank, size, err) / norm2(b, rank, size, err)) > tol;
        deleteVector(r1, err);
        r1 = r2;
    }
    deleteVector(z, err);
    deleteVector(r1, err);
    return x;
}


double calErrInf(ellipticProb* ell, vector* v, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
                 myError* err) {
    if (ell == NULL || v == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = ell->n - 2;
    double startX = ell->sq.leftBottom.x;
    double startY = ell->sq.leftBottom.y;
    double sizeLen = ell->sq.sideLength;
    vector* X = linspace(startX, startX + sizeLen, ell->n, allocator, err);
    if (X == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    vector* Y = linspace(startY, startY + sizeLen, ell->n, allocator, err);
    if (Y == NULL) {
        deleteVector(X, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }

    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) столбцов
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k столбцов.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);

    double valLocal = 0;
    double valGlobal;
    size_t counter = rank * k * dim;
    for (size_t col = start; col < end; ++col) {
        for (size_t row = 0; row < dim; ++row) {
            double x = X->data[col + 1];
            double y = Y->data[row + 1];
            double tmp = fabs(u(x, y) - v->data[counter]);
            if (tmp > valLocal)
                valLocal = tmp;
            counter++;
        }
    }
    MPI_Allreduce(&valLocal, &valGlobal, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    deleteVector(X, err);
    deleteVector(Y, err);
    return valGlobal;
}


void deleteEllipticProb(ellipticProb* ell, myError* err) {
    if (ell == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(ell->indexLeft);
    free(ell);
}
