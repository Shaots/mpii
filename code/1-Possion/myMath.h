//
// Created by Lenovo on 25.10.2023.
//
#pragma once
#ifndef PARALAB_MYMATH_H
#define PARALAB_MYMATH_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vector.h"
#include "sparse.h"
#include "square.h"


vector*
linspace(double start, double end, size_t num, void* const (* allocator)(size_t, size_t, myError*), myError* err);


double normInf(vector* vec, myError* err);


double norm2(vector* vec, int rank, int size, myError* err);


#endif //PARALAB_MYMATH_H