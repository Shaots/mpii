//
// Created by Lenovo on 14.11.2023.
//

#include "mpi_test.h"

void runMPI(int argc, char** argv){
    int rc;
    rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS) {
        printf("Initialization failed with error code %d\n", rc);
        MPI_Abort(MPI_COMM_WORLD, rc);
    }
    int rank;
    int size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    myError* err;
    FILE* errorFile;
    err = calloc(1, sizeof(myError));
    errorFile = fopen("err.txt", "ab");
    setLogger(errorFile);


    test(rank, size, err);


    free(err);
    fclose(errorFile);
    MPI_Finalize();
}
