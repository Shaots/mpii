//
// Created by Lenovo on 21.10.2023.
//

#include "vector.h"

vector* creatVector(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (allocator == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* vec = (vector*) allocator(1, sizeof(vector), err);
    if (vec == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vec->data = (double*) allocator(n, sizeof(double), err);
    if (vec->data == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        free(vec);
        return NULL;
    }
    vec->n = n;
    return vec;
}


void copyData(vector* from, vector* to, myError* err) {
    if (from->n != to->n) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    memcpy(to->data, from->data, from->n * sizeof(double));
}


double dotProduct_series(vector* v1, vector* v2, myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    double sum = 0;
    for (size_t i = 0; i < dim; ++i) {
        sum = sum + v1->data[i] * v2->data[i];
    }
    return sum;
}


double dotProduct_para(vector* v1, vector* v2, int rank, int size, myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) строк
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k строк.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);
    double sumGlobal;
    double sumLocal = 0;
    for (size_t i = start; i < end; ++i) {
        sumLocal += v1->data[i] * v2->data[i];
    }
    MPI_Allreduce(&sumLocal, &sumGlobal, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return sumGlobal;
}


// z = ax + y
vector*
saxpy_series(double a, vector* v1, vector* v2, void* const (* allocator)(size_t, size_t, myError*), myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* z = creatVector(dim, allocator, err);
    if (z == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    for (size_t i = 0; i < dim; ++i) {
        z->data[i] = a * v1->data[i] + v2->data[i];
    }
    return z;
}


vector*
saxpy_para(double a, vector* v1, vector* v2, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
           myError* err) {
    if (v1 == NULL || v2 == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t dim = v1->n;
    if (v2->n != dim) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    vector* zGlobal = creatVector(dim, allocator, err);
    if (zGlobal == NULL) {
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t r = dim % size;       // Последний процесс берет ответственность на (k + r) строк
    size_t k = (dim - r) / size; // Каждый процесс берет ответственность на k строк.
    size_t start = rank * k;
    size_t end = (rank + 1) * k + r * (rank == size - 1);
    size_t num = end - start;


    int* displacement = allocator(size, sizeof(int), err);
    if (displacement == NULL) {
        deleteVector(zGlobal, err);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    int* receiveCounts = allocator(size, sizeof(int), err);
    if (receiveCounts == NULL) {
        deleteVector(zGlobal, err);
        free(receiveCounts);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }

    receiveCounts[0] = (int) k;
    displacement[0] = 0;
    for (size_t i = 1; i < size; ++i) {
        receiveCounts[i] = (int) (k + r * (i == size - 1));
        displacement[i] = displacement[i - 1] + receiveCounts[i - 1];
    }

    vector* zLocal = creatVector(num, allocator, err);
    if (zLocal == NULL) {
        deleteVector(zGlobal, err);
        free(receiveCounts);
        free(displacement);
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NULL;
    }
    size_t counter = 0;
    for (size_t i = start; i < end; ++i) {
        zLocal->data[counter++] = a * v1->data[i] + v2->data[i];
    }

    MPI_Allgatherv(zLocal->data, (int)num, MPI_DOUBLE,
                   zGlobal->data, receiveCounts, displacement, MPI_DOUBLE, MPI_COMM_WORLD);
    deleteVector(zLocal, err);
    free(displacement);
    free(receiveCounts);
    return zGlobal;
}


void deleteVector(vector* vec, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    free(vec->data);
    free(vec);
}


void paintVector(vector* vec, myError* err) {
    if (vec == NULL) {
        *err = ERROR_INVALID_ARGUMENT;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    size_t dim = vec->n;
    printf("(");
    for (size_t i = 0; i < dim - 1; ++i) {
        printf("%lf    ", vec->data[i]);
    }
    printf("%lf)\n", vec->data[dim - 1]);
}
