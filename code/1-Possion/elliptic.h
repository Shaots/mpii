//
// Created by Lenovo on 21.10.2023.
//
#pragma once
#ifndef PARALAB_ELLIPTIC_H
#define PARALAB_ELLIPTIC_H

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <windows.h>
#include <dos.h>
#include <unistd.h>
#include "sparse.h"
#include "vector.h"
#include "square.h"
#include "myMath.h"

#define SHAOTRUE 1
#define SHAOFAUSE 0


#if SIZE_MAX == UCHAR_MAX
#define MY_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
#define MY_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
#define MY_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
#define MY_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
#define MY_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#else
#error "what is happening here?"
#endif


// - div (grad u) = f;
// x \in square
// u = u0 in boundary

// numeration:
// n = 5
// 04 09 14 19 24
// 03 08 13 18 23
// 02 07 12 17 22
// 01 06 11 16 21
// 00 05 10 15 20

// [06, 07, 08, 11, 12, 13, 16, 17, 18] --- unknown. (n - 2)^2
// [00, 01, 02, 03, 04, 05, 09, 10, 14, 19, 20, 21, 22, 23, 24] -- known 4(n - 1) from boundary condition
// [06, 08, 16, 18] --- angle: 4
// [07] --- left:     (n - 4)
// [11] --- down:     (n - 4)
// [13] --- up:       (n - 4)
// [17] --- right:    (n - 4)
// [12] --- internal: (n - 4)^2

// Now we only need to creat the matrix with size (n - 2)^2! NOT n^2
// 02 05 08
// 01 04 07
// 00 03 06
// [00, 02, 06, 08] --- angle: 4
// [01] --- left:     (n - 4)
// [03] --- down:     (n - 4)
// [05] --- up:       (n - 4)
// [07] --- right:    (n - 4)
// [04] --- internal: (n - 4)^2


#define PI 3.1415926535897932384626433832795028841971

typedef struct indexAngle {
    size_t leftDown;
    size_t leftUp;
    size_t rightDown;
    size_t rightUp;
} indexAngle;

typedef struct ellipticProb {
    square sq;
    size_t n;
    double h;

    indexAngle iA;
    size_t* indexLeft;
    size_t* indexDown;
    size_t* indexUp;
    size_t* indexRight;
} ellipticProb;


double u(double x, double y);


double f(double x, double y);


ellipticProb*
creatEllipticProb(square sq, size_t n, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
                  myError* err);


sparseMatrix*
calStiffness(ellipticProb* ell, int rank, int size, void* const (* allocator)(size_t, size_t, myError*), myError* err);


vector*
calForce(ellipticProb* ell, int rank, int size, void* const (* allocator)(size_t, size_t, myError*), myError* err);


vector* fiveDiagMatrixMultiplyVec(ellipticProb* ell, sparseMatrix* A, vector* vec, int rank, int size,
                                  void* const (* allocator)(size_t, size_t, myError*), myError* err);

// solver for system of linear equations. Ax = b
vector* jacobi(ellipticProb* ell, sparseMatrix* A, vector* b, int rank, int size,
               void* const (* allocator)(size_t, size_t, myError*), myError* err);

// solver for system of lin equations. Ax = b
vector* conjGrad(ellipticProb* ell, sparseMatrix* A, vector* b, int rank, int size,
                 void* const (* allocator)(size_t, size_t, myError*), myError* err);


double calErrInf(ellipticProb* ell, vector* v, int rank, int size, void* const (* allocator)(size_t, size_t, myError*),
                 myError* err);


void deleteEllipticProb(ellipticProb* ell, myError* err);


#endif //PARALAB_ELLIPTIC_H
