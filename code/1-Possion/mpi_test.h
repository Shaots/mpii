//
// Created by Lenovo on 14.11.2023.
//

#ifndef PARALAB_MPI_TEST_H
#define PARALAB_MPI_TEST_H

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "test.h"
#include "../error.h"

void runMPI(int argc, char** argv);

#endif //PARALAB_MPI_TEST_H
