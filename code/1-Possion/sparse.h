//
// Created by Lenovo on 20.10.2023.
//

#ifndef PARALAB_SPARSE_H
#define PARALAB_SPARSE_H

#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "../error.h"

typedef struct sparseMatrix {
    vector* vec;
    size_t* rowIndex;
    size_t* colIndex;
} sparseMatrix;


sparseMatrix* createSparseMatrix(size_t n, void* const (* allocator)(size_t, size_t, myError*), myError* err);


void sparseSetCoord(sparseMatrix* sp, size_t index, size_t row, size_t col, double value, myError* err);


void deleteSparseMatrix(sparseMatrix* sp, myError* err);


void paintSparse(sparseMatrix* sp, myError* err);


#endif //PARALAB_SPARSE_H