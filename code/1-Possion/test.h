//
// Created by Lenovo on 27.10.2023.
//

#ifndef PARALAB_TEST_H
#define PARALAB_TEST_H


#include <stdio.h>
#include <stdlib.h>
#include "../error.h"
#include "square.h"
#include "vector.h"
#include "sparse.h"
#include "myMath.h"
#include "elliptic.h"


typedef double mytime;


void paintArr(size_t* arr, size_t n);


void testSquare();


void testVector(int rank, int size, myError* err);


void testMatrixVector(int rank, int size, myError* err);


void testPriori(int rank, int size, myError* err);


void testRuntime1(int rank, int size, myError* err);


void test(int rank, int size, myError* err);

#endif //PARALAB_TEST_H
