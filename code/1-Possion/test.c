//
// Created by Lenovo on 27.10.2023.
//
#include "test.h"


void paintArr(size_t* arr, size_t n) {
    printf("(");
    for (size_t i = 0; i < n - 1; ++i) {
        printf("%zd    ", arr[i]);
    }
    printf("%zd)\n", arr[n - 1]);
}


void testSquare() {
    printf("Test square:\n");
    point p1;
    p1.x = -1;
    p1.y = -1;
    double sideLen1 = 5;
    square sq1 = creatSquare(p1, sideLen1);
    paintSquare(sq1);

    point p2;
    p2.x = 1;
    p2.y = 1;
    double sideLen2 = -5;
    square sq2 = creatSquare(p2, sideLen2);
    paintSquare(sq2);
    printf("\n\n");
}


void testVector(int rank, int size, myError* err) {
    if (rank == 0) {
        printf("Test vector:\n");
    }
    size_t len1 = 5;
    vector* vec1 = creatVector(len1, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    if (rank == 0) {
        paintVector(vec1, err);
        printf("Length of vector: %zd\n", vec1->n);
    }
    for (size_t i = 0; i < len1; ++i) {
        vec1->data[i] = (double) (i * i);
    }
    double vec1Vec1 = dotProduct_para(vec1, vec1, rank, size, err);
    double normVec1 = norm2(vec1, rank, size, err);

    if (rank == 0) {
        paintVector(vec1, err);
        printf("(v, v) = %lf\n", vec1Vec1);
        printf("||v||_{2} = %lf\n", normVec1);
    }

    double alpha = -1;
    vector* vec2 = saxpy_para(alpha, vec1, vec1, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                              err);
    if (rank == 0) {
        printf("%lf * vec1 + vec1 = \n", alpha);
        paintVector(vec2, err);
    }
    deleteVector(vec1, err);
    deleteVector(vec2, err);
    if (rank == 0) {
        printf("\n\n");
    }
}


void testMatrixVector(int rank, int size, myError* err) {
    if (rank == 0) {
        printf("Test matrix * vector Ax:\n");
    }
    point p1;
    p1.x = 0;
    p1.y = 0;
    double sideLen = 1;
    square sq = creatSquare(p1, sideLen);
    size_t n = 7;
    if (size > n - 2) {
        printf("Too more processes\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    size_t dim = n - 2;
    ellipticProb* ell = creatEllipticProb(sq, n, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                                          err);
    sparseMatrix* stiff = calStiffness(ell, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    vector* vec = creatVector(dim * dim, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    for (int i = 0; i < dim * dim; ++i) {
        vec->data[i] = 1;
    }
    vector* res = fiveDiagMatrixMultiplyVec(ell, stiff, vec, rank, size,
                                            (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    if (rank == 0) {
        paintVector(res, err);
    }
    deleteEllipticProb(ell, err);
    deleteVector(vec, err);
    deleteVector(res, err);
    deleteSparseMatrix(stiff, err);
}


void testPriori(int rank, int size, myError* err) {
    FILE* res = fopen("result time.txt", "ab");
    if (rank == 0) {
        fprintf(res, "Test priori estimate:\n");
        fprintf(res, "Num of parallel processes: Size = %d\n", size);
    }

    point p1;
    p1.x = 0;
    p1.y = 0;
    double sideLen = 1;
    square sq = creatSquare(p1, sideLen);
    size_t n[5] = {17, 33, 65, 129, 257};
    for (size_t i = 0; i < 5; ++i) {
        if (size > n[i] - 2) {
            printf("Too more processes\n");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        ellipticProb* ell = creatEllipticProb(sq, n[i], rank, size,
                                              (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        sparseMatrix* stiff = calStiffness(ell, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                                           err);
        vector* force = calForce(ell, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        vector* v = jacobi(ell, stiff, force, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                           err);
        double errInf = calErrInf(ell, v, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        if (rank == 0) {
            fprintf(res, "Mesh size: h = 1/%zd    ||u - v||_{inf} = %g\n", n[i] - 1, errInf);
        }
        deleteEllipticProb(ell, err);
        deleteSparseMatrix(stiff, err);
        deleteVector(force, err);
        deleteVector(v, err);
    }
    fclose(res);
}


void testRuntime1(int rank, int size, myError* err) {
    FILE* res = fopen("result time.txt", "ab");
    if (rank == 0) {
        fprintf(res, "Dependence of execution time on task size.\n");
        fprintf(res, "Num of parallel processes: Size = %d\n", size);
    }
    point p1;
    p1.x = 0;
    p1.y = 0;
    double sideLen = 1;
    square sq = creatSquare(p1, sideLen);
    size_t n[1];
    for (int i = 0; i < 1; ++i) {
        n[i] = 66 + i;
    }
    size_t times = 10;
    for (size_t i = 0; i < 1; ++i) {
        mytime start, end;
        start = MPI_Wtime();
        for (size_t j = 0; j < times; ++j) {
            if (size > n[i] - 2) {
                printf("Too more processes\n");
                MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
            }
            ellipticProb* ell = creatEllipticProb(sq, n[i], rank, size,
                                                  (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            sparseMatrix* stiff = calStiffness(ell, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                                               err);
            vector* force = calForce(ell, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            vector* v = jacobi(ell, stiff, force, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory,
                               err);
            deleteEllipticProb(ell, err);
            deleteSparseMatrix(stiff, err);
            deleteVector(force, err);
            deleteVector(v, err);
            //MPI_Barrier(MPI_COMM_WORLD);
        }
        end = MPI_Wtime();
        if (rank == 0) {
            //printf("Task size M * M = %zd * %zd,    time = %.8f\n", n[i], n[i], (end - start) / (double) times);
            fprintf(res, "%.8f\n", (end - start) / (double) times);
        }
    }
    size_t s;
    MPI_Allreduce(&memorySize, &s, 1, MY_MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);
    if (rank == 0)
        fprintf(res, "size of memory = %lf (MB)\n", (double) s / (double) times / 1024 / 1024);
    fclose(res);
}


void test(int rank, int size, myError* err) {
    // testVector(rank, size, err);
    // testMatrixVector(rank, size, err);
    testRuntime1(rank, size, err);
    testPriori(rank, size, err);
}