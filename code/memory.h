//
// Created by Lenovo on 21.10.2023.
//

#ifndef PARALAB_MEMORY_H
#define PARALAB_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include "error.h"


extern size_t memorySize;


void* allocMemory(size_t numOfElement, size_t sizeOfElement, myError* err);


#endif //PARALAB_MEMORY_H
