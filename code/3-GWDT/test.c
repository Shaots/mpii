//
// Created by PC on 06.01.2024.
//

#include "test.h"
#include "gwdt.h"
#include "pgm.h"

void test(const char* filename, int rank, int size, myError* err) {
    pgm* myPGM = readPGM(filename, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
    if (myPGM == NULL) {
        *err = ERROR_FILE_CANNOT_BE_OPENED;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return;
    }
    GWDT* nextLayer;
    GWDT* previousLayer;
    double start, end;
    int times = 50;
    start = MPI_Wtime();

    for (int i = 0; i < times; ++i) {
        nextLayer = createGWDT(myPGM->width, myPGM->height,
                               (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        nextLayer = initGWDT(nextLayer, rank, size, (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        previousLayer = createGWDT(myPGM->width, myPGM->height,
                                   (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
        copyGWDT(nextLayer, previousLayer, err);
        size_t n = 0;
        int flag = 1;
        while (flag) {
            nextLayer = eikonal(myPGM, previousLayer, nextLayer, rank, size,
                                (void* const (*)(size_t, size_t, myError*)) allocMemory, err);
            flag = compare2Layer(previousLayer, nextLayer, rank, size, err);
            /*if (rank == 0) {
                printf("%d\n", flag);
            }*/
            copyGWDT(nextLayer, previousLayer, err);
            n++;
        }
        deleteGWDT(nextLayer, err);
        deleteGWDT(previousLayer, err);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    end = MPI_Wtime();
    FILE* file = fopen("result.txt", "ab");
    fprintf(file, "%lf\n", (end - start) / times);

    /*if (rank == 0) {
        pgm* out = scale(nextLayer, err);
        //paintPGM(out, err);
        writePGM(out, "out.pgm", err);
        deletePGM(out, err);
    }*/

    deletePGM(myPGM, err);
    /*deleteGWDT(nextLayer, err);
    deleteGWDT(previousLayer, err);*/

}