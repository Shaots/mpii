//
// Created by Lenovo on 06.01.2024.
//

#include "mmath.h"

double findMin(const double* arr, size_t len, myError* err) {
    if (arr == NULL) {
        *err = ERROR_NULL;
        reportError(*err, __FILE__, __FUNCTION__, __LINE__);
        return NAN;
    }
    double value = arr[0];
    for (size_t i = 1; i < len; ++i) {
        if (arr[i] < value)
            value = arr[i];
    }
    return value;
}