//
// Created by PC on 06.01.2024.
//

#ifndef PARAMAIN_TEST_H
#define PARAMAIN_TEST_H

#include <mpi.h>
#include "../error.h"



void test(const char* filename, int rank, int size, myError* err);


#endif //PARAMAIN_TEST_H
