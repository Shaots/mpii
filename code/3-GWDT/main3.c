//
// Created by Lenovo on 21.11.2023.
//

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "pgm.h"
#include "test.h"


int main(int argc, char** argv) {
    int rc;
    rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS) {
        printf("Initialization failed with error code %d\n", rc);
        MPI_Abort(MPI_COMM_WORLD, rc);
    }
    int rank;
    int size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);


    myError* err;
    FILE* errorFile;
    err = calloc(1, sizeof(myError));
    errorFile = fopen("err.txt", "ab");
    setLogger(errorFile);

    const char* filename = "chess.pgm";
    int width = 1920;
    int height = 1080;
    if (rank == 0) {
        creatChess(filename, width, height);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    test(filename, rank, size, err);


    free(err);
    fclose(errorFile);
    MPI_Finalize();
    return 0;
}
